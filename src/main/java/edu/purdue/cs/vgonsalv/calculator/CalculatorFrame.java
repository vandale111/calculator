/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.calculator;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Vic
 */
public class CalculatorFrame extends JFrame{

    public CalculatorFrame()  {
        super();
        initComponents();
    }
    private void initComponents(){
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 400);
    }
    
    public static void main(String[] args){
        SwingUtilities.invokeLater(()->{
            CalculatorFrame f = new CalculatorFrame();
            f.setVisible(true);
        });
    }
}
