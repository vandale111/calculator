/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.calculator;

import java.math.BigDecimal;

/**
 *
 * @author Vic
 */
public class Constant implements Evaluatable{
    BigDecimal val;

    Constant(BigDecimal val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val.toPlainString();
    }

    @Override
    public BigDecimal evaluate() {
        return val;
    }
}
