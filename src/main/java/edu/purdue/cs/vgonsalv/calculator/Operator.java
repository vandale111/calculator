/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 *
 * @author Vic
 */
public enum Operator implements Evaluatable{
    ADD("+", 1,(a,b) -> a.add(b,getContext())),
    SUBTRACT("-",1, (a,b) -> a.subtract(b,getContext())),
    MULTIPLY("*",2, (a,b) -> a.multiply(b,getContext())),
    DIVIDE("/", 2,(a,b) -> a.divide(b,getContext()));
    String strVal;
    int precedence;
    private static MathContext context = new MathContext(13, RoundingMode.HALF_EVEN);
    BiFunction<BigDecimal, BigDecimal, BigDecimal> func;
    Evaluatable left,right;
    private Operator(String strVal,int precedence,BiFunction<BigDecimal, BigDecimal, BigDecimal> func){
        this.strVal = strVal;
        this.func = func;
        this.precedence = precedence;
    }

    public static MathContext getContext() {
        return context;
    }

    public static void setContext(MathContext context) {
        Operator.context = context;
    }
    
    
    @Override
    public BigDecimal evaluate() {
        return func.apply(left.evaluate(),right.evaluate());
    }
    public static  Optional<Operator> lookup(String s){
        return Arrays.stream(values()).filter(op -> op.strVal.equals(s)).findAny();
    }
}
