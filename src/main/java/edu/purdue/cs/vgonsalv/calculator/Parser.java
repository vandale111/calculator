/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.calculator;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Vic
 */
public class Parser {

    public static BigDecimal parse(String input) {

        ArrayDeque<Evaluatable> stack = new ArrayDeque<>();

        Constant result;
        try {
            lex(input).forEachRemaining(token -> {
                if (stack.isEmpty()) {
                    stack.push(token);
                    return;
                }
                if (token instanceof Operator) {
                    if (stack.size() >= 2) {
                        if (stack.peek() instanceof Constant) {
                            Constant right = (Constant) stack.pop();
                            Operator onStack = (Operator) stack.pop();
                            Constant left = (Constant) stack.pop();
                            Operator next = (Operator) token;
                            if (onStack.precedence >= next.precedence) {
                                onStack.left = left;
                                onStack.right = right;
                                stack.push(new Constant(onStack.evaluate()));
                            } else {
                                stack.push(left);
                                stack.push(onStack);
                                stack.push(right);
                            }
                        }
                    }
                    stack.push(token);
                } else {
                    Evaluatable top = stack.pop();
                    if (top instanceof Operator
                            &&(stack.isEmpty()? true:stack.peek() instanceof Operator)) {
                        if (top == Operator.ADD) {
                        } else if (top == Operator.SUBTRACT) {
                            Operator op = Operator.SUBTRACT;
                            op.left = new Constant(BigDecimal.ZERO);
                            op.right = token;
                            token = new Constant(op.evaluate());
                        } else {
                            throw new ClassCastException();
                        }
                    } else {
                        stack.push(top);
                    }
                    stack.push(token);
                }
            });
            result = (Constant) stack.pop();
            while (!stack.isEmpty()) {
                Operator onStack = (Operator) stack.pop();
                Constant left = (Constant) stack.pop();
                onStack.left = left;
                onStack.right = result;
                result = new Constant(onStack.evaluate());

            }
            return result.evaluate();
        } catch (ClassCastException | NoSuchElementException | NullPointerException e) {
            return null;
        }
    }

    private static enum TokenType {

        OPERATOR("[*/+-]"),
        NUMBER("[0-9]+([.][0-9]*)?|[.][0-9]+"),
        WHITESPACE("[ \t\f\r\n]+");

        public final String pattern;
        private static final Pattern tokenPatterns;

        static {
            StringJoiner tokenPatternBuffer = new StringJoiner("|");
            for (TokenType tokenType : TokenType.values()) {
                tokenPatternBuffer.add(String.format("(?<%s>%s)", tokenType.name(), tokenType.pattern));
            }
            tokenPatterns = Pattern.compile(tokenPatternBuffer.toString());
        }

        private TokenType(String pattern) {
            this.pattern = pattern;
        }
    }

    private static Iterator<Evaluatable> lex(String s) {
        Matcher matcher = TokenType.tokenPatterns.matcher(s);
        return new Iterator<Evaluatable>() {
            Evaluatable next = null;
            boolean finished = false;

            @Override
            public boolean hasNext() {
                if (finished) {
                    return false;
                }
                if (next == null) {
                    updateNext();
                }
                return !(finished);
            }

            @Override
            public Evaluatable next() {
                if (next == null && !finished) {
                    updateNext();
                }
                if (finished) {
                    throw new NoSuchElementException();
                }
                Evaluatable temp = next;
                next = null;
                return temp;
            }

            private void updateNext() {
                boolean skip;
                try {
                    do {
                        matcher.find();

                        if (matcher.group(TokenType.WHITESPACE.name()) != null) {
                            skip = true;
                            continue;
                        }
                        skip = false;
                        String result;
                        if ((result = matcher.group(TokenType.OPERATOR.name())) != null) {
                            next = Operator.lookup(result).get();
                        } else if ((result = matcher.group(TokenType.NUMBER.name())) != null) {
                            next = new Constant(new BigDecimal(result));
                        } else if (next == null) {
                            throw new IllegalArgumentException("invalid token in stream");
                        }
                    } while (skip);
                } catch (IllegalStateException e) {
                    finished = true;
                }
            }

        };
    }
}
