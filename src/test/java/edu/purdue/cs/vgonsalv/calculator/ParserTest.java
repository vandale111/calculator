/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * Version 2, December 2004 
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * 0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package edu.purdue.cs.vgonsalv.calculator;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vic
 */
public class ParserTest {
    
    public ParserTest() {
    }

    @Test
    public void testParse_01() {
        assertEquals(new BigDecimal("1"), Parser.parse("1"));
    }
    @Test
    public void testParse_02() {
        assertEquals(new BigDecimal("2"), Parser.parse("1+1"));
    }
    @Test
    public void testParse_03() {
        assertEquals(new BigDecimal("2"), Parser.parse("1 + 1"));
    }
    @Test
    public void testParse_04() {
        assertEquals(new BigDecimal("2"), Parser.parse("1 +\n1\n"));
    }
    @Test
    public void testParse_05() {
        assertEquals(new BigDecimal("4"), Parser.parse("1--3"));
    }
    @Test
    public void testParse_06() {
        assertEquals(new BigDecimal("7"), Parser.parse("1+2*3"));
    }
    @Test
    public void testParse_07() {
        assertEquals(new BigDecimal("106"), Parser.parse("5*20+2*3"));
    }
    @Test
    public void testParse_08() {
        assertEquals(new BigDecimal("106.00"),Parser.parse("5.0*20.0+2.0*3.0"));
    }
    @Test
    public void testParse_09() {
        assertEquals(new BigDecimal("0.5"), Parser.parse("1-.5"));
    }
    @Test
    public void testParse_10() {
        assertEquals(new BigDecimal("0.3333333333333"), Parser.parse("1/3"));
    }
    
}
